#include <stdio.h>
#include <stdlib.h>
#include "../src/ramer_douglas_peucker.h"

#define RED "\033[31m"
#define GREEN "\033[32m"

char compare(point have[], const int expected[], int size)
{
    int i;
    for (i=0;i<size;i++) {
        if (have[i].hidden != expected[i]) {
            return 0;
        }
    }

    return 1;
}

void test1()
{
    size_t lines = 4;
    point path[lines];
    point p = {150, 10, 0};
    path[0] = p;
    p.x = 200;
    p.y = 100;
    path[1] = p;
    p.x = 360;
    p.y = 170;
    path[2] = p;
    p.x = 500;
    p.y = 280;
    path[3] = p;

    size_t removed = rdp_reduce(path, lines, 30);
    if (removed != 1) {
        printf(RED "Error test1 removed %zu instead 1\n", removed);
        return;
    }

    int expected[4] = {0,0,1,0};
    if (!compare(path, expected, lines)) {
        printf(RED "Error test1 not expected result\n");
        return;
    }

    printf(GREEN "test1 ok\n");
}

void test2()
{
    size_t lines = 4;
    point path[lines];
    point p = {25, 25, 0};
    path[0] = p;
    p.x = 37;
    p.y = 62;
    path[1] = p;
    p.x = 75;
    p.y = 75;
    path[2] = p;
    p.x = 100;
    p.y = 100;
    path[3] = p;

    size_t removed = rdp_reduce(path, lines, 10);
    if (removed != 1) {
        printf(RED "Error test2 removed %zu instead 1\n", removed);
        return;
    }

    int expected[4] = {0,0,1,0};
    if (!compare(path, expected, lines)) {
        printf(RED "Error test2 not expected result\n");
        return;
    }

    printf(GREEN "test2 ok\n");
}

void test3()
{
    char name[5] = "test3";
    size_t lines = 6;
    point path[lines];
    point p = {-30, -40, 0};
    path[0] = p;
    p.x = -20;
    p.y = -10;
    path[1] = p;
    p.x = 10;
    p.y = 10;
    path[2] = p;
    p.x = 50;
    p.y = 0;
    path[3] = p;
    p.x = 40;
    p.y = -30;
    path[4] = p;
    p.x = 10;
    p.y = -40;
    path[5] = p;

    size_t removed = rdp_reduce(path, lines, 12);
    if (removed != 1) {
        printf(RED "Error %s removed %zu instead 1\n", name, removed);
        return;
    }

    int expected[6] = {0,1,0,0,0,0};
    if (!compare(path, expected, lines)) {
        printf(RED "Error %s not expected result\n", name);
        return;
    }

    printf(GREEN "%s ok\n", name);
}

void test4()
{
    char name[5] = "test4";
    size_t lines = 6;
    point path[lines];
    point p = {-30, -40, 0};
    path[0] = p;
    p.x = -20;
    p.y = -10;
    path[1] = p;
    p.x = 10;
    p.y = 10;
    path[2] = p;
    p.x = 50;
    p.y = 0;
    path[3] = p;
    p.x = 40;
    p.y = -30;
    path[4] = p;
    p.x = 10;
    p.y = -40;
    path[5] = p;

    size_t removed = rdp_reduce(path, lines, 15);
    if (removed != 2) {
        printf(RED "Error %s removed %zu instead 2\n", name, removed);
        return;
    }

    int expected[6] = {0,1,0,0,1,0};
    if (!compare(path, expected, lines)) {
        printf(RED "Error %s not expected result\n", name);
        return;
    }

    printf(GREEN "%s ok\n", name);
}

void test5()
{
    char name[5] = "test5";
    size_t lines = 6;
    point path[lines];
    point p = {-30, -40, 0};
    path[0] = p;
    p.x = -20;
    p.y = -10;
    path[1] = p;
    p.x = 10;
    p.y = 10;
    path[2] = p;
    p.x = 50;
    p.y = 0;
    path[3] = p;
    p.x = 40;
    p.y = -30;
    path[4] = p;
    p.x = 10;
    p.y = -40;
    path[5] = p;

    size_t removed = rdp_reduce(path, lines, 40);
    if (removed != 3) {
        printf(RED "Error %s removed %zu instead 3\n", name, removed);
        return;
    }

    int expected[6] = {0,1,0,1,1,0};
    if (!compare(path, expected, lines)) {
        printf(RED "Error %s not expected result\n", name);
        return;
    }

    printf(GREEN "%s ok\n", name);
}

void test6()
{
    char name[5] = "test6";
    size_t lines = 6;
    point path[lines];
    point p = {-30, -40, 0};
    path[0] = p;
    p.x = -20;
    p.y = -10;
    path[1] = p;
    p.x = 10;
    p.y = 10;
    path[2] = p;
    p.x = 50;
    p.y = 0;
    path[3] = p;
    p.x = 40;
    p.y = -30;
    path[4] = p;
    p.x = 10;
    p.y = -40;
    path[5] = p;

    size_t removed = rdp_reduce(path, lines, 50);
    if (removed != 4) {
        printf(RED "Error %s removed %zu instead 4\n", name, removed);
        return;
    }

    int expected[6] = {0,1,1,1,1,0};
    if (!compare(path, expected, lines)) {
        printf(RED "Error %s not expected result\n", name);
        return;
    }

    printf(GREEN "%s ok\n", name);
}

void test7()
{
    char name[5] = "test7";
    size_t lines = 4;
    point path[lines];
    point p = {0.0034, 0.013, 0};
    path[0] = p;
    p.x = 0.0048;
    p.y = 0.006;
    path[1] = p;
    p.x = 0.0062;
    p.y = 0.01;
    path[2] = p;
    p.x = 0.0087;
    p.y = 0.009;
    path[3] = p;

    size_t removed = rdp_reduce(path, lines, 0.001);
    if (removed != 0) {
        printf(RED "Error %s removed %zu instead 0\n", name, removed);
        return;
    }

    printf(GREEN "%s ok\n", name);
}

void test8()
{
    char name[5] = "test8";
    size_t lines = 4;
    point path[lines];
    point p = {0.0034, 0.013, 0};
    path[0] = p;
    p.x = 0.0048;
    p.y = 0.006;
    path[1] = p;
    p.x = 0.0062;
    p.y = 0.01;
    path[2] = p;
    p.x = 0.0087;
    p.y = 0.009;
    path[3] = p;

    size_t removed = rdp_reduce(path, lines, 0.003);
    if (removed != 1) {
        printf(RED "Error %s removed %zu instead 1\n", name, removed);
        return;
    }

    int expected[6] = {0,0,1,0};
    if (!compare(path, expected, lines)) {
        printf(RED "Error %s not expected result\n", name);
        return;
    }

    printf(GREEN "%s ok\n", name);
}

void test9()
{
    char name[5] = "test9";
    size_t lines = 4;
    point path[lines];
    point p = {0.0034, 0.013, 0};
    path[0] = p;
    p.x = 0.0048;
    p.y = 0.006;
    path[1] = p;
    p.x = 0.0062;
    p.y = 0.01;
    path[2] = p;
    p.x = 0.0087;
    p.y = 0.009;
    path[3] = p;

    size_t removed = rdp_reduce(path, lines, 0.01);
    if (removed != 2) {
        printf(RED "Error %s removed %zu instead 2\n", name, removed);
        return;
    }

    int expected[6] = {0,1,1,0};
    if (!compare(path, expected, lines)) {
        printf(RED "Error %s not expected result\n", name);
        return;
    }

    printf(GREEN "%s ok\n", name);
}

void test10()
{
    char name[6] = "test10";
    size_t lines = 9;
    point path[lines];
    point p = {-43, 8, 0};
    path[0] = p;
    p.x = -24;
    p.y = 19;
    path[1] = p;
    p.x = -13;
    p.y = 23;
    path[2] = p;
    p.x = -8;
    p.y = 36;
    path[3] = p;
    p.x = 7;
    p.y = 40;
    path[4] = p;
    p.x = 24;
    p.y = 12;
    path[5] = p;
    p.x = 44;
    p.y = -6;
    path[6] = p;
    p.x = 57;
    p.y = 2;
    path[7] = p;
    p.x = 70;
    p.y = 7;
    path[8] = p;

    size_t removed = rdp_reduce(path, lines, 10);
    if (removed != 5) {
        printf(RED "Error %s removed %zu instead 5\n", name, removed);
        return;
    }

    int expected[9] = {0,1,1,1,0,1,0,1,0};
    if (!compare(path, expected, lines)) {
        printf(RED "Error %s not expected result\n", name);
        return;
    }

    printf(GREEN "%s ok\n", name);
}

int main() {
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    test7();
    test8();
    test9();
    test10();
}
