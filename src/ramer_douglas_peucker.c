#include <math.h>
#include "ramer_douglas_peucker.h"

double intersection(point p, double line_slope)
{
    return p.y - (line_slope * p.x);
}

double perpendicular_distance(point p, line ln, double line_slope)
{
    if (fabs(line_slope) == INFINITY) {
        return fabs(p.x - ln.ini.x);
    }

    return fabs((line_slope * p.x) - p.y + intersection(ln.ini, line_slope))
           / sqrt(line_slope * line_slope + 1);
}

size_t rdp_reduce(point *path, size_t size, double tolerance)
{
    if (size < 3) {
        return 0;
    }

    line ln = {path[0], path[size-1]};
    double line_slope = slope(ln);

    size_t removed = 0;
    double max_dist = 0;
    size_t index = 0;

    int i;
    for (i=1; i<size-1; i++) {
        double dist = perpendicular_distance(path[i], ln, line_slope);
        if (dist > max_dist) {
            index = i;
            max_dist = dist;
        }
    }

    if (max_dist > tolerance) {
        removed += rdp_reduce(&path[0], index+1, tolerance);
        removed += rdp_reduce(&path[index], size-index, tolerance);
    } else {
        for (i=1;i<size-1;i++) {
            path[i].hidden=1;
            removed++;
        }
    }

    return removed;
}
