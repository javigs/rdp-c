#ifndef RDP_RAMER_DOUGLAS_PEUCKER_H
#define RDP_RAMER_DOUGLAS_PEUCKER_H

#include <stdlib.h>
#include "point.h"
#include "line.h"

double intersection(point p, double line_slope);

double perpendicular_distance(point p, line ln, double line_slope);

size_t rdp_reduce(point *path, size_t size, double tolerance);

#endif //RDP_RAMER_DOUGLAS_PEUCKER_H
