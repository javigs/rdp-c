#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "tsv_gps.h"
#include "ramer_douglas_peucker.h"

int main(int argc, char *argv[]) {
    if (argc < 3) {
        printf("Params: <TSV filename> <tolerance> [<output filename>]\n");
        exit(EXIT_FAILURE);
    }

    size_t lines = count_lines(argv[1]) - 1; // Minus CSV header
    double tolerance = strtod(argv[2], NULL);

    if (lines <= 1) {
        printf("No file, bad format or empty data\n");
        exit(EXIT_FAILURE);
    }

    printf("INPUT\n");
    printf("- File points: %zu\n", lines);
    printf("- Tolerance selected: %lf\n", tolerance);

    point *path = tsv_read_points(argv[1], lines);

    clock_t start = clock();
    size_t removed = rdp_reduce(path, lines, tolerance);
    clock_t end = clock();
    double elapsed = (double)(end - start)/(double)CLOCKS_PER_SEC;

    printf("RESULT:\n");
    printf("- Removed points: %zu\n", removed);
    printf("- Elapsed time: %lfs\n\n", elapsed);

    if (argc > 3 && argv[3]) {
        tsv_write_points(argv[3], path, lines - 1);
    }
    free(path);

    exit(EXIT_SUCCESS);
}
