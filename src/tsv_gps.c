#include "tsv_gps.h"

int count_lines(char *file)
{
    int lines = 0;
    FILE *fp;

    if ((fp = fopen(file, "r"))) {
        char line[32];
        while (fgets(line, 32, fp)) {
            if (line[0] != '\n') {
                lines++;
            }
        }
        fclose(fp);
    }

    return lines;
}

point create_point(double lat, double lon)
{
    point p = {lat, lon, 0};

    return p;
}

point * tsv_read_points(char *file, size_t lines)
{
    FILE *fp = fopen(file, "r");
    if (fp == NULL) {
        return NULL;
    }

    point *path = malloc(lines * sizeof(point));
    if (path == NULL) {
        return NULL;
    }

    size_t i = 0;
    char line[32];
    char *ptr;
    double lon, lat;
    // Skip first line
    fgets(line, 32, fp);
    while (fgets(line, 32, fp) != NULL && i < (lines - 1)) {
        if (line[0] != '\n') {
            lat = strtod(line, &ptr);
            lon = strtod(ptr, &ptr);
            path[i++] = create_point(lat, lon);
        }
    }

    return path;
}

void tsv_write_points(char *filename, point *path, size_t size) {
    FILE *fp = fopen(filename, "w");
    if (fp == NULL) {
        printf("Can't write file result");
        exit(EXIT_FAILURE);
    }

    fprintf(fp, "latitude\tlongitude\n");
    int i = 0;
    for(; i < size ; i++) {
        if (path[i].hidden == 0) {
            fprintf(fp, "%.*f\t%.*f\n", 9, path[i].x, 9, path[i].y);
        }
    }
    fclose(fp);
}
