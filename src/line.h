#ifndef RDP_LINE_H
#define RDP_LINE_H

#include "point.h"

typedef struct {
    point ini;
    point end;
} line;

double slope(line ln);

#endif //RDP_LINE_H
