#include <math.h>
#include <float.h>
#include "line.h"

double slope(line ln)
{
    if (fabs(ln.end.x - ln.ini.x) < FLT_EPSILON) {
        if (ln.end.x > ln.ini.x) {
            return INFINITY;
        } else {
            return -INFINITY;
        }
    }

    return (ln.end.y - ln.ini.y) / (ln.end.x - ln.ini.x);
}
