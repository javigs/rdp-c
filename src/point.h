#ifndef RDP_POINT_H
#define RDP_POINT_H

typedef struct {
    double x;
    double y;
    int hidden;
} point;

#endif //RDP_POINT_H

