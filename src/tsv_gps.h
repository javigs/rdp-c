#ifndef RDP_TSV_GPS_H
#define RDP_TSV_GPS_H

#include <stdio.h>
#include <stdlib.h>
#include "point.h"

int count_lines(char *file);

point * tsv_read_points(char *file, size_t lines);

void tsv_write_points(char *filename, point *path, size_t size);

#endif //RDP_TSV_GPS_H
